[![build status](https://gitlab.com/ci/projects/7310/status.png?ref=master)](https://gitlab.com/ci/projects/7310?ref=master)

## 준비
  * node-pushserver

## 설치
```
git clone git@gitlab.com:umven/node-pushserver.git
cd node-pushserver
npm install -g
```

## 실행
```
export WEB_PORT={port_number}
export GCM_KEY={gcm_key}
export PUSH_DB=mongodb://localhost/{db_name}
export CERT_PATH=cert/{project_name}/development/cert.pem
export KEY_PATH=cert/{project_name}/development/key.pem
export GATEWAY=gateway.sandbox.push.apple.com
export FEEDBACK=feedback.sandbox.push.apple.com
node ./bin/pushserver.js -c ./config.json
```
